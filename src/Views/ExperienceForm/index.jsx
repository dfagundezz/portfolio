import React, { useContext } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { ButtonOutline } from '../../Components/ButtonOutline';
import { useState } from 'react';
import List from '../../Components/List';
import { uuidv4 as uuid } from '@firebase/util';
import { useEffect } from 'react';

function ExperienceForm() {
	const { globalData, setGlobalData, nextStep, prevStep } =
		useContext(PotatoContext);

	const [experiences, setExperiences] = useState(globalData.portfolio.experience ?? []);
	const [newExperience, setNewExperience] = useState({
		name: '',
		reference: '',
		description: '',
		startDate: '',
		endDate: '',
	});

	useEffect(
		() =>
			setGlobalData((prevData) => ({
				...prevData,
				portfolio: { ...prevData.portfolio, experience: experiences },
			})),
		[experiences]
	);

  const deleteExperience = (id) => {
    let exps = experiences.slice();
    let index = exps.findIndex((ex) => ex.id === id);
    if (index > -1) {
      exps.splice(index, 1);
    }
    setExperiences(exps);
  }


	return (
		<div>
			<SimpleTitle text="Experience" />
			<Input
				label="Company Name"
				input={{
					id: 'name',
					name: 'name',
					type: 'text',
					onChange: (e) => {
						setNewExperience((prevState) => ({
							...prevState,
							name: e.target.value,
						}));
					},
					value: newExperience.name,
				}}
			/>

			<Input
				label="Position"
				input={{
					id: 'position',
					name: 'position',
					onChange: (e) => {
						setNewExperience((prevState) => ({
							...prevState,
							reference: e.target.value,
						}));
					},
					value: newExperience.reference,
				}}
			/>

			<Input
				label="Description"
				input={{
					id: 'description',
					name: 'description',
					onChange: (e) => {
						setNewExperience((prevState) => ({
							...prevState,
							description: e.target.value,
						}));
					},
					value: newExperience.description,
				}}
			/>
      
			<Input
				label="From"
				input={{
					id: 'startDate',
					name: 'startDate',
					type: 'date',
					onChange: (e) => {
						setNewExperience((prevState) => ({
							...prevState,
							startDate: e.target.value,
						}));
					},
					value: newExperience.startDate,
				}}
			/>
			<Input
				label="To"
				input={{
					id: 'endDate',
					name: 'endDate',
					type: 'date',
					onChange: (e) => {
						setNewExperience((prevState) => ({
							...prevState,
							endDate: e.target.value,
						}));
					},
					value: newExperience.endDate,
				}}
			/>

			<ButtonSolid
				disabled={
					newExperience.name.length === 0 ||
					newExperience.reference.length === 0 ||
					newExperience.startDate.length === 0
				}
				text="Add experience"
				callback={() => {
					setExperiences((prevState) => [
						...prevState,
						{ ...newExperience, id: uuid() },
					]);

					setNewExperience({
						name: '',
						reference: '',
						description: '',
						startDate: '',
						endDate: '',
					});

					setGlobalData(() => ({
						...globalData,
						experience: experiences,
					}));
				}}
			/>

			<List deleteItem={deleteExperience} items={experiences} />

			<ButtonSolid
				text="Continue"
				disabled={globalData.step === 7}
				callback={nextStep}
			/>
			<ButtonOutline
				text="Back"
				disabled={globalData.step === 0}
				callback={prevStep}
			/>
		</div>
	);
}

export { ExperienceForm };
