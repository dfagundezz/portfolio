import React, { useContext } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { ButtonOutline } from '../../Components/ButtonOutline';
import { useState } from 'react';
import { useEffect } from 'react';
import { uuidv4 as uuid } from '@firebase/util';
import List from '../../Components/List';

function EducationForm() {
	const { globalData, setGlobalData, nextStep, prevStep } =
		useContext(PotatoContext);

	const [educations, setEducations] = useState(
		globalData.portfolio.education ?? []
	);
	const [newEd, setNewEd] = useState({
		name: '',
		reference: '',
		description: '',
		startDate: '',
		endDate: '',
	});

	useEffect(
		() =>
			setGlobalData((prevData) => ({
				...prevData,
				portfolio: { ...prevData.portfolio, education: educations },
			})),
		[educations]
	);

	const deleteEducation = (id) => {
		let eds = educations.slice();
		let index = eds.findIndex((ex) => ex.id === id);
		if (index > -1) {
			eds.splice(index, 1);
		}
		setEducations(eds);
	};

	return (
		<div>
			<SimpleTitle text="Education" />
			<Input
				label="Title"
				input={{
					id: 'name',
					name: 'name',
					type: 'text',
					onChange: (e) => {
						setNewEd((prevState) => ({
							...prevState,
							name: e.target.value,
						}));
					},
					value: newEd.name,
				}}
			/>

			<Input
				label="Institution"
				input={{
					id: 'institution',
					name: 'institution',
					onChange: (e) => {
						setNewEd((prevState) => ({
							...prevState,
							reference: e.target.value,
						}));
					},
					value: newEd.reference,
				}}
			/>

			<Input
				label="Description"
				input={{
					id: 'description',
					name: 'description',
					onChange: (e) => {
						setNewEd((prevState) => ({
							...prevState,
							description: e.target.value,
						}));
					},
					value: newEd.description,
				}}
			/>

			<Input
				label="From"
				input={{
					id: 'startDate',
					name: 'startDate',
					type: 'date',
					onChange: (e) => {
						setNewEd((prevState) => ({
							...prevState,
							startDate: e.target.value,
						}));
					},
					value: newEd.startDate,
				}}
			/>
			<Input
				label="To"
				input={{
					id: 'endDate',
					name: 'endDate',
					type: 'date',
					onChange: (e) => {
						setNewEd((prevState) => ({
							...prevState,
							endDate: e.target.value,
						}));
					},
					value: newEd.endDate,
				}}
			/>

			<ButtonSolid
				disabled={
					newEd.name.length === 0 ||
					newEd.reference.length === 0 ||
					newEd.startDate.length === 0
				}
				text="Add education"
				callback={() => {
					setEducations((prevState) => [
						...prevState,
						{ ...newEd, id: uuid() },
					]);

					setNewEd({
						name: '',
						reference: '',
						description: '',
						startDate: '',
						endDate: '',
					});
				}}
			/>

			<List deleteItem={deleteEducation} items={educations} />

			<ButtonSolid
				text="Continue"
				disabled={globalData.step === 7}
				callback={nextStep}
			/>
			<ButtonOutline
				text="Back"
				disabled={globalData.step === 0}
				callback={prevStep}
			/>
		</div>
	);
}

export { EducationForm };
