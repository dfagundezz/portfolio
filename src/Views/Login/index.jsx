import React, { useContext, useState } from 'react';
import { ButtonOutline } from '../../Components/ButtonOutline';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import Password from '../../Components/Password/Password';
import { SimpleTitle } from '../../Components/SimpleTitle';

import './Login.css';

function Login({ setLoginState }) {
	const { globalData, setGlobalData, nextStep, loginUser } =
		useContext(PotatoContext);

	const [user, setUser] = useState({ fullName: '', email: '', password: '' });

	const handleSubmit = async (e) => {
		e.preventDefault();
		setGlobalData((prevState) => ({
			...prevState,
			user: { ...prevState.user, ...user },
		}));
		try {
			await loginUser(user.email, user.password);
			nextStep();
		} catch (error) {
			console.log(error.code);
		}
	};

	const handleChangeName = (event) => {
		setUser({ ...user, fullName: event.target.value });
	};

	const handleChange = (e) => {
		setUser({ ...user, [e.target.name]: e.target.value });
		setGlobalData({
			...globalData,
			...user,
			[e.target.name]: e.target.value,
		});
	};

	return (
		<div className="LoginView">
			<form onSubmit={handleSubmit}>
				<SimpleTitle text="Welcome back!" />
				<div className="holder">
					<Input
						input={{
							name: 'name',
							placeholder: 'Full name',
							onChange: handleChangeName,
							value: user.name,
						}}
					/>
					<Input
						input={{
							name: 'email',
							type: 'email',
							placeholder: 'Email address',
							autoComplete: 'email',
							value: user.email,
							onChange: handleChange,
						}}
					/>
					<Password
						input={{
							id: 'password',
							name: 'password',
							placeholder: '·······',
							value: user.password,
							onChange: handleChange,
						}}
						icon="fa-lock"
					/>
				</div>
				<div className="holder">
					<ButtonSolid text="Log In" type="submit" />
					<ButtonOutline
						text="Not account yet? Register!"
						callback={(e) => {
							e.preventDefault();
							setLoginState(false);
						}}
					/>
				</div>
			</form>
		</div>
	);
}

export { Login };
