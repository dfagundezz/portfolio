import React, { useContext } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { ButtonOutline } from '../../Components/ButtonOutline';
import { useState } from 'react';
import { useEffect } from 'react';
import List from '../../Components/List';
import {uuidv4 as uuid} from '@firebase/util';

function SkillsForm() {
	const { globalData, setGlobalData, nextStep, prevStep } =
		useContext(PotatoContext);

	const [skills, setSkills] = useState(globalData.portfolio.skills ?? []);
	const [newSkill, setNewSkill] = useState({
		name: '',
		percentage: '',
		type: '',
	});

	useEffect(
		() =>
			setGlobalData((prevData) => ({
				...prevData,
				portfolio: { ...prevData.portfolio, skills: skills },
			})),
		[skills]
	);

	const deleteSkill = (id) => {
		let sk = skills.slice();
		let index = sk.findIndex((ex) => ex.id === id);
		if (index > -1) {
			sk.splice(index, 1);
		}
		setSkills(sk);
	};

	return (
		<div>
			<SimpleTitle text="Skills" />

			<Input
				label="Skill Name"
				input={{
					id: 'name',
					name: 'name',
					type: 'text',
					onChange: (e) => {
						setNewSkill((prevState) => ({
							...prevState,
							name: e.target.value,
						}));
					},
					value: newSkill.name,
				}}
			/>

			<Input
				label="Level"
        phrase="0 to 100"
				input={{
					id: 'level',
					name: 'level',
					onChange: (e) => {
						setNewSkill((prevState) => ({
							...prevState,
							percentage: e.target.value > 100 ? 100 : e.target.value,
						}));
					},
          pattern: "[0-9]{3}",
          min: 1,
          defaultValue: 50,
          max: 100,
          type: 'number' ,
					value: newSkill.reference,
				}}
			/>

			<Input
				label="Type"
        phrase="Example: Programming, Arts & Crafts, etc"
				input={{
					id: 'type',
					name: 'type',
					onChange: (e) => {
						setNewSkill((prevState) => ({
							...prevState,
							type: e.target.value,
						}));
					},
					value: newSkill.description,
				}}
			/>

			<ButtonSolid
				disabled={
					newSkill.name.length === 0 ||
					newSkill.percentage.length === 0 ||
					newSkill.type.length === 0
				}
				text="Add skill"
				callback={() => {
					setSkills((prevState) => [
						...prevState,
						{ ...newSkill, id: uuid() },
					]);

					setNewSkill({
						name: '',
						percentage: '',
						type: '',
					});
				}}
			/>

			<List deleteItem={deleteSkill} items={skills} />

			<ButtonSolid
				text="Continue"
				disabled={globalData.step === 6}
				callback={nextStep}
			/>
			<ButtonOutline
				text="Back"
				disabled={globalData.step === 0}
				callback={prevStep}
			/>
		</div>
	);
}

export { SkillsForm };
