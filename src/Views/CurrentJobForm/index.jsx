import React, { useContext, useState } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { ButtonOutline } from '../../Components/ButtonOutline';

function CurrentJobForm() {
	const { globalData, setGlobalData, nextStep, prevStep, cancel } =
		useContext(PotatoContext);

	const [currentJob, setCurrentJob] = useState(
		globalData.user.currentJob ?? ''
	);

	const handleChange = (event) => {
		event.preventDefault();
		setCurrentJob(event.target.value);
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		if (
			currentJob.trim().length > 0 &&
			currentJob !== globalData.user.currentJob
		) {
			setGlobalData((prevState) => ({
				...prevState,
				user: {
					...prevState.user,
					currentJob: currentJob,
				},
			}));
		}

		nextStep();
	};

	return (
		<form onSubmit={handleSubmit}>
			<SimpleTitle text="Current Job" />
			<Input
				input={{
					name: 'currentJob',
					type: 'text',
					placeholder: currentJob,
					onChange: handleChange,
					maxlength: 35,
				}}
				phrase="Just the title of your current job"
			/>
			<ButtonSolid
				text="Continue"
				disabled={globalData.step === 7}
				type="submit"
			/>
			<ButtonOutline
				text="Back"
				disabled={globalData.step === 0}
				callback={prevStep}
			/>
		</form>
	);
}

export { CurrentJobForm };
