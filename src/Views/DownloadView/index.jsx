import jsPDF from 'jspdf';
import React from 'react';
import { useContext } from 'react';
import { ButtonOutline } from '../../Components/ButtonOutline';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { PotatoContext } from '../../Components/PotatoContext';
import { SimpleTitle } from '../../Components/SimpleTitle';

const DownloadView = () => {
    const {prevStep, globalData} = useContext(PotatoContext);

	function convert (e){
		e.preventDefault();
		const experience = [globalData.portfolio.experience];
		const education = [globalData.portfolio.education];
		const skills = [globalData.portfolio.skills];

		var doc = new jsPDF('p', 'in', 'a4');
			doc.setDrawColor("#644ad4");
			doc.setLineWidth( 1.25 / 72);
			doc.line(0.5, 0.5, 0.5, 11.25);
			let verticalOffset = 0.5;
			doc.setFontSize(13);
			doc.text(0.6,verticalOffset += 0.25 , '- ABOUT ME - \n');
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(11);
			doc.text(0.6,verticalOffset += 0.25 , "Full Name: " + globalData.user.fullName);
			doc.setFontSize(11);
			doc.text(0.6,verticalOffset += 0.25 , "Contact e-mail: " + globalData.user.email);
			doc.setFontSize(11);
			doc.text(0.6,verticalOffset += 0.25, "Current Job: " + globalData.user.currentJob);
			doc.setFontSize(11);
			const longText = doc.splitTextToSize( 'About: ' + globalData.user.about, 7.25)
			doc.text(0.6,verticalOffset + 11 / 72 + 0.2, longText);
			verticalOffset += (longText.length + 0.5) * 11 / 72;

			doc.setLineWidth( 1 / 72);
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.line(0.6, verticalOffset += 0.30, verticalOffset, verticalOffset);
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(13);
			doc.text(0.6,verticalOffset += 0.25,'- EXPERIENCE - \n');
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(11);
			experience.forEach((element) =>{
				element.forEach((exp)=>{
					doc.text(0.6,verticalOffset += 0.25, "Name: " + exp.name + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Reference: " + exp.reference  + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Description: " + exp.description + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Date: " + exp.startDate + ' to ' + exp.startDate + '\n');
					doc.text(0.6,verticalOffset += 0.25,'\n');
				})
			});

			doc.addPage();
			doc.setDrawColor("#644ad4");
			doc.setLineWidth( 1.25 / 72);
			doc.line(0.5, 0.5, 0.5, 11.25);
			verticalOffset = 0.5;
			doc.setLineWidth( 1 / 72);
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(13);
			doc.text(0.6,verticalOffset += 0.25,'- EDUCATION - \n');
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(11);

			education.forEach((element) =>{
				element.forEach((edu)=>{
					doc.text(0.6,verticalOffset += 0.25, "Name: " + edu.name + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Reference: " + edu.reference  + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Description: " + edu.description + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Date: " + edu.startDate + ' to ' + edu.startDate + '\n');
					doc.text(0.6,verticalOffset += 0.25,'\n');
				})
			});

			doc.setLineWidth( 1 / 72);
			doc.line(0.6, verticalOffset += 0.30, verticalOffset, verticalOffset);
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(13);
			doc.text(0.6,verticalOffset += 0.25,'- SKILLS - \n');
			doc.text(0.6,verticalOffset += 0.25,'\n');
			doc.setFontSize(11);

			skills.forEach((element) =>{
				element.forEach((skill)=>{
					doc.text(0.6,verticalOffset += 0.25, "Name: " + skill.name + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Type: " + skill.type  + '\n');
					doc.text(0.6,verticalOffset += 0.25, "Description: " + skill.description + '\n');
					doc.text(0.6,verticalOffset += 0.25,'\n');
				})
			});

			doc.save('cv.pdf');
	}
	return (
		<div>
			<SimpleTitle text="Download your portfolio!" />
			<ButtonSolid text="Download"  callback={convert}/>
			<ButtonOutline text="Back" callback={prevStep} />
		</div>
	);
};

export default DownloadView;
