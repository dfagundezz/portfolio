import React, { useContext, useState } from 'react';
import { Input } from '../../Components/Input';
import { PotatoContext } from '../../Components/PotatoContext';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { ButtonOutline } from '../../Components/ButtonOutline';
import './register.css';

function Register({ setLoginState }) {
	const { setGlobalData, nextStep, registerUser } =
		useContext(PotatoContext);
	const [formState, setFormState] = useState({
		fullName: '',
		email: '',
		password: '',
	});

	const onChangeFullName = (event) => {
		setFormState({ ...formState, fullName: event.target.value });
	};
	const onChangeEmail = (event) => {
		setFormState({ ...formState, email: event.target.value.trim() });
	};
	const onChangePassword = (event) => {
		setFormState({ ...formState, password: event.target.value });
	};

	const onSubmit = async (event) => {
		event.preventDefault();
		// Tricky: Primero usamos el spread operator ... en el estado anterior para mantener esas propiedades.
		// Como vamos a modificar el objeto "user" dentro del estado global, tenemos que acceder a este.
		// Para no perder TODOS los valores dentro de globalData.user,
		// tenemos que hacer un spread del estado anterior de la propiedad user
		// Por eso indicamos que user es un objeto, el cual contiene
		// -> las propiedades del objeto anterior + las propiedades que agregamos del formState
		setGlobalData((prevState) => ({
			...prevState,
			user: { ...prevState.user, ...formState },
			step: prevState.step + 1,
		}));
		try {
			await registerUser(formState.email, formState.password);
			nextStep();
		} catch (error) {
			console.log(error.code);
		}
	};

	return (
		<>
			<form onSubmit={onSubmit}>
				<SimpleTitle text="Registrate para obtener una cuenta" />
				<Input
					input={{
						placeholder: 'Full name',
						onChange: onChangeFullName,
						value: formState.fullName,
					}}
					label=""
					icon=""
					phrase=""
				/>
				<Input
					input={{
						placeholder: 'Email',
						onChange: onChangeEmail,
						value: formState.email,
					}}
					label=""
					icon=""
					phrase=""
				/>
				<Input
					input={{
						placeholder: 'Password',
						onChange: onChangePassword,
						type: 'password',
						value: formState.password,
					}}
					label=""
					icon=""
					phrase=""
				/>
				<ButtonSolid
					text="SIGN UP"
					icon=""
					type="submit"
					disabled={false}
					onClick={false}
				/>
				<ButtonOutline
					text="Sign up with LinkedIn"
					icon=""
					type="button"
					disabled={false}
				/>
			</form>
			<p>
				Ya tienes una cuenta?
				<button
					className="btn-login"
					onClick={(e) => {
						e.preventDefault();
						setLoginState(true);
					}}>
					Ingresar
				</button>
			</p>
		</>
	);
}

export { Register };
