import React, { useContext } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { ButtonOutline } from '../../Components/ButtonOutline';
import { useState } from 'react';
import { useEffect } from 'react';
import List from '../../Components/List';
import { uuidv4 as uuid } from '@firebase/util';
function ProjectsForm() {
	const { globalData, setGlobalData, prevStep, nextStep } =
		useContext(PotatoContext);

	const [projects, setProjects] = useState(
		globalData.portfolio.projects ?? []
	);
	const [newProject, setNewProject] = useState({
		name: '',
		description: '',
	});

	useEffect(
		() =>
			setGlobalData((prevData) => ({
				...prevData,
				portfolio: { ...prevData.portfolio, projects: projects },
			})),
		[projects]
	);

	const deleteProject = (id) => {
		let proj = projects.slice();
		let index = proj.findIndex((ex) => ex.id === id);
		if (index > -1) {
			proj.splice(index, 1);
		}
		setProjects(proj);
	};

	return (
		<div>
			<SimpleTitle text="Projects" />
			<Input
				label="Name"
				input={{
					id: 'name',
					name: 'name',
					type: 'text',
					onChange: (e) => {
						setNewProject((prevState) => ({
							...prevState,
							name: e.target.value,
						}));
					},
					value: newProject.name,
				}}
			/>

			<Input
				label="Description"
				input={{
					id: 'description',
					name: 'description',
					onChange: (e) => {
						setNewProject((prevState) => ({
							...prevState,
							description: e.target.value,
						}));
					},
					value: newProject.description,
				}}
			/>

			<ButtonSolid
				disabled={
					newProject.name.length === 0
				}
				text="Add Project"
				callback={() => {
					setProjects((prevState) => [
						...prevState,
						{ ...newProject, id: uuid() },
					]);

					setNewProject({
						name: '',
						reference: '',
						description: '',
					});
				}}
			/>

			<List deleteItem={deleteProject} items={projects} />
			<ButtonSolid callback={nextStep} text="Continue" />
			<ButtonOutline
				text="Back"
				disabled={globalData.step === 0}
				callback={prevStep}
			/>
		</div>
	);
}

export { ProjectsForm };
