import React, { useContext, useState } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { Input } from '../../Components/Input';
import { ButtonSolid } from '../../Components/ButtonSolid';

function FullNameForm() {
	const { globalData, setGlobalData, nextStep } =
		useContext(PotatoContext);

	const [newFullName, setNewFullName] = useState('');

	const onChangeFullName = (event) => {
		setNewFullName(event.target.value);
	};
	const onSubmit = (event) => {
		event.preventDefault();

		// Si el nuevo nombre completo NO esta vacío (inicializa en '')
		// y ES DISTINTO al global, lo cambiamos
		if (
			newFullName.trim().length > 0 &&
			newFullName !== globalData.user.fullName
		) {
			setGlobalData((prevState) => ({
				...prevState,
				user: {
					...prevState.user,
					fullName: newFullName,
				},
			}));
		}

		nextStep();
	};
	return (
		<form onSubmit={onSubmit}>
			<SimpleTitle text="Full name" />
			<Input
				input={{
					name: 'fullname',
					type: 'text',
					placeholder: globalData.user.fullName,
					value: newFullName,
					onChange: onChangeFullName,
					maxlength: 40
				}}
				phrase="Feel free to change your name 😉"
			/>

			<ButtonSolid
				disabled={
					newFullName.trim().length === 0 &&
					globalData.user.fullName.trim().length === 0
				}
				text="Continue"
				type="submit"
			/>
		</form>
	);
}

export { FullNameForm };
