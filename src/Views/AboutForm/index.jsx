import React, { useContext, useState } from 'react';
import { SimpleTitle } from '../../Components/SimpleTitle';
import { PotatoContext } from '../../Components/PotatoContext';
import { ButtonSolid } from '../../Components/ButtonSolid';
import { ButtonOutline } from '../../Components/ButtonOutline';
import TextArea from '../../Components/TextArea/TextArea';
function AboutForm() {
	const { globalData, setGlobalData, nextStep, prevStep, cancel } =
		useContext(PotatoContext);

	const [formState, setFormState] = useState(globalData.user.about ?? '');

	const handleChange = (e) => {
		e.preventDefault();
		let val = e.target.value;
		setFormState(val);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		setGlobalData((prevState) => ({
			...prevState,
			user: { ...prevState.user, about: formState },
		}));
		nextStep();
	};

	return (
		<form style={{display: 'flex', flexDirection: 'column', alignItems: 'center', gap: '5px'}} onSubmit={handleSubmit}>
			<SimpleTitle text="About" />
			<TextArea
				input={{
					col: 40,
					rows: 4,
					id: 'about',
					name: 'about',
					onChange: handleChange,
					value: formState,
				}}
			/>
			<p>Tell us a little bit more about you</p>
			<ButtonSolid
				text="Continue"
				disabled={globalData.step === 7}
				type="submit"
				callback={handleSubmit}
			/>
			<ButtonOutline
				text="Back"
				disabled={globalData.step === 0}
				callback={prevStep}
			/>
		</form>
	);
}

export { AboutForm };
