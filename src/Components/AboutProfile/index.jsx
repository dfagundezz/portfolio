import React from 'react';
import './AboutProfile.css';

function AboutProfile(props) {
  return (
    <div className="AboutProfile">
      <h2>About</h2>
      <p className="about--text">{props.aboutText}</p>
    </div>
  );
}

export { AboutProfile };
