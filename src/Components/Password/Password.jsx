import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './Password.css';

const Password = ({ input, label, icon }) => {
	const [display, setDisplay] = useState(false);

	const handleClick = (evt) => {
		evt.preventDefault();
		setDisplay(!display);
	};

	return (
		<div className="input">
			{label && <label htmlFor={input.id}>{label}</label>}
			<div className="controls">
				{icon && (
					<label className="icon" htmlFor={input.id}>
						<i className={'fa ' + icon} aria-hidden="true"></i>
					</label>
				)}

				<input
					className="control password"
					type={display ? 'text' : 'password'}
					{...input}
				/>
				<button className="show-password" onClick={handleClick}>
					<i
						className={
							'fa fa-' + (display ? 'eye-slash' : 'eye')
						}></i>
				</button>
			</div>
		</div>
	);
};

Password.propTypes = {
	input: PropTypes.object.isRequired,
	label: PropTypes.string,
	icon: PropTypes.string,
};

export default Password;
