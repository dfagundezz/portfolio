import React from 'react';
import './SectionProfile.css';

function SectionProfile({ title, data }) {
	const showDate = (value) => {
		if (
			value.startDate === '' ||
			value.startDate === null ||
			value.startDate === undefined
		)
			return '';
		return (
			<>
				| ({value.startDate} →{' '}
				{value.endDate !== '' ? value.endDate : 'present'}){' '}
			</>
		);
	};

	return (
		<div className="sectionProfileWrapper">
			<div className="header">{title}</div>
			{data.map((value) => (
				<div key={value.id} className="sectionProfileContentWrapper">
					<img height="100px" src={value.image} alt="" />
					<div className="sectionProfileContent">
						<div>
							<h5>
								{value.name} {value.reference !== '' && value.reference!== undefined ? '|' : ''} {value.reference}{' '}
								{showDate(value)}
							</h5>
						</div>
						<div>{value.description}</div>
					</div>
				</div>
			))}
		</div>
	);
}

export { SectionProfile };
