import React from 'react';
import './ButtonSolid.css';

function ButtonSolid({ text, icon, type, disabled, callback }) {
  return (
    <button
      className="ButtonSolid"
      onClick={callback}
      type={type ?? 'button'}
      disabled={disabled}
    >
      {icon && <i className={'fa ' + (icon ? icon : '')}></i>}
      <span>{text}</span>
    </button>
  );
}

export { ButtonSolid };
