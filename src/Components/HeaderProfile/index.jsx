import React from 'react';
import './HeaderProfile.css';

function HeaderProfile(props) {
  const [imageProfile, setImageProfile] = React.useState(null);
  const [imageBanner, setImageBanner] = React.useState(null);
  const onImageProfileChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImageProfile(URL.createObjectURL(img));
    }
  };
  const onImageBannerChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImageBanner(URL.createObjectURL(img));
    }
  };
  return (
    <div
      style={{ backgroundImage: `url(${imageBanner})` }}
      className="profile-banner shadow-sm"
    >
      <div className="profile-photo">
        <img className="shadow-img" src={imageProfile} alt="" />
        <h2 className='fullname--text'>
          <strong>{props.fullName}</strong>
        </h2>
        <h3>{props.currentJob}</h3>
        <label className="add-photo-profile shadow-md">
          <input type="file" onChange={onImageProfileChange} />+
        </label>
      </div>
      <label className="add-photo-banner shadow-md">
        <input type="file" onChange={onImageBannerChange} />+
      </label>
    </div>
  );
}

export { HeaderProfile };
