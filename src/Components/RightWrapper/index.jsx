import React from 'react';
import './RightRectangle.css';

function RightWrapper(props) {
  return (
    <section className="rightRectangle">
      <div className="preview">{props.children}</div>
    </section>
  );
}

export { RightWrapper };
