import React, { useContext } from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';
import { PotatoContext } from '../PotatoContext';
import { LeftWrapper } from '../LeftWrapper';
import { RightWrapper } from '../RightWrapper';
import { HeaderProfile } from '../HeaderProfile';
import { AboutProfile } from '../AboutProfile';
import { SectionProfile } from '../SectionProfile';
import { SkillsProfile } from '../SkillsProfile';
import './App.css';

function AppUI() {
	const { globalData, percentage } = useContext(PotatoContext);
	return (
		<React.Fragment>
			<LeftWrapper />
			<RightWrapper>
				{globalData.error && <p>Hubo un error...</p>}
				{globalData.loading && <p>Estamos cargando...</p>}
				{globalData.user.fullName && (
					<HeaderProfile
						fullName={globalData.user.fullName}
						currentJob={globalData.user.currentJob}
					/>
				)}

				{globalData.user.about && (
					<AboutProfile aboutText={globalData.user.about} />
				)}

				{globalData.portfolio.experience?.length > 0 && (
					<SectionProfile
						title="Experience"
						data={globalData.portfolio.experience}
					/>
				)}

				{globalData.portfolio.education?.length > 0 && (
					<SectionProfile
						title="Education"
						data={globalData.portfolio.education}
					/>
				)}

				{globalData.portfolio.skills?.length > 0 && (
					<SkillsProfile
						title="Skills"
						data={globalData.portfolio.skills}
					/>
				)}

				{globalData.portfolio.projects?.length > 0 && (
					<SectionProfile
						title="Projects"
						data={globalData.portfolio.projects}
					/>
				)}
				{globalData.step > 0 && (
					<ProgressBar animated now={percentage} />
				)}
			</RightWrapper>
		</React.Fragment>
	);
}

export { AppUI };
