import React from 'react';
import { PotatoProvider } from '../PotatoContext';
import { AppUI } from '.';
import {Route, Routes} from 'react-router-dom';
import AboutUs from '../../pages/AboutUs/aboutUs';
import Faqs from '../../pages/Faqs/Faqs';
import Contact from '../../pages/Contact/Contact';
import DropDownMenu from '../DropDownMenu/DropDownMenu';

function App() {
  return (
    <>
    <DropDownMenu/>
      <Routes>
          <Route path="/" element={
            <PotatoProvider>
              <AppUI />
            </PotatoProvider> }
          />
          <Route path="/about-us" element={<AboutUs/>}/>
          <Route path="/faqs" element={<Faqs/>}/>
          <Route path="/contact" element={<Contact/>}/>
      </Routes>
    </>
  );
}

export { App };
