import React, { useContext } from 'react';
import './LeftRectangle.css';
import { PotatoContext } from '../PotatoContext';
import { Register } from '../../Views/Register';
import { FullNameForm } from '../../Views/FullNameForm';
import { CurrentJobForm } from '../../Views/CurrentJobForm';
import { AboutForm } from '../../Views/AboutForm';
import { ExperienceForm } from '../../Views/ExperienceForm';
import { EducationForm } from '../../Views/EducationForm';
import { SkillsForm } from '../../Views/SkillsForm';
import { ProjectsForm } from '../../Views/ProjectsForm';
import { Login } from '../../Views/Login';
import { useState } from 'react';
import DownloadView from '../../Views/DownloadView';

function LeftWrapper() {
	const { globalData } = useContext(PotatoContext);

	const [loginState, setLoginState] = useState(false);

	const pageDisplay = (currentStep) => {
		let rs = null;
		switch (currentStep) {
			case 1:
				if (loginState)
					rs = <Login setLoginState={setLoginState} />;
					else rs = <Register setLoginState={setLoginState} />;
				break;
			case 2:
				rs = <FullNameForm />;
				break;
			case 3:
				rs = <CurrentJobForm />;
				break;
			case 4:
				rs = <AboutForm />;
				break;
			case 5:
				rs = <ExperienceForm />;
				break;
			case 6:
				rs = <EducationForm />;
				break;
			case 7:
				rs = <SkillsForm />;
				break;
			case 8:
				rs = <ProjectsForm />;
				break;
			case 9:
				rs = <DownloadView />;
				break;
			default:
				rs = <p>404.Not found!</p>;
				break;
		}

		return rs;
	};

	return (
		<section className="leftRectangle">
			{pageDisplay(globalData.step)}
		</section>
	);
}

export { LeftWrapper };
