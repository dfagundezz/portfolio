import React from 'react'
import './List.css';

const List = ({items, deleteItem}) => {
  return (
    <div className='List'>
        <ul>
            {items.map(item => (
                <li onDoubleClick={() => deleteItem(item.id)} className='ListItem' key={item.id}>{item.name}</li>
            ))}
        </ul>
    </div>
  );
}

export default List;