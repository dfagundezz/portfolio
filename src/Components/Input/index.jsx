import React from 'react';
import PropTypes from 'prop-types';
import './Input.css';

function Input({ input, label, icon, phrase }) {
  return (
    <div className="InputWrapper">
      {label && <label htmlFor={input.id}>{label}</label>}
      <div className="InputControls">
        {icon && (
          <label className="InputIcon" htmlFor={input.id}>
            <i className={'fa ' + icon} aria-hidden="true"></i>
          </label>
        )}

        <input className="InputControl" {...input} />
      </div>
      <p>{phrase}</p>
    </div>
  );
}

Input.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  icon: PropTypes.string,
};

export { Input };
