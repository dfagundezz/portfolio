import React from 'react';
import PropTypes from 'prop-types';
import './SimpleTitle.css';

function SimpleTitle({ text }) {
	return <h1 className="title">{text}</h1>;
}

// El valor del texto es requerido para usar el component. ¿Qué sentido tendría usar el título sin un texto 👀 ?
SimpleTitle.propTypes = {
	text: PropTypes.string.isRequired,
};

export { SimpleTitle };
