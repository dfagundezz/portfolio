import React, { createContext, useState } from 'react';
import { auth } from '../../firebase/firebase';
import {
	createUserWithEmailAndPassword,
	signInWithEmailAndPassword,
	setPersistence,
	browserLocalPersistence,
} from 'firebase/auth';

const PotatoContext = createContext();

const data = {
	user: {},
	portfolio: {},
	error: false,
	loading: false,
	step: auth.currentUser ? 2 : 1,
	isLogged: auth.currentUser ? true : false,
};

const PotatoProvider = ({ children }) => {
	const [globalData, setGlobalData] = useState(data);
	const [percentage, setPercentage] = useState(0);
	const [user, setUser] = useState(false);

	const percentageStep = 100 / 7;
	const nextStep = () => {
		setPercentage(percentageStep * (globalData.step + 1));
		setGlobalData((prevData) => ({ ...prevData, step: prevData.step + 1 }));
	};
	const prevStep = () => {
		setPercentage(percentageStep * (globalData.step - 1));
		setGlobalData((prevData) => ({ ...prevData, step: prevData.step - 1 }));
	};
	const cancel = () => {
		setPercentage(percentageStep * 0);
		setGlobalData((prevData) => ({ ...prevData, step: 0 }));
	};

	const registerUser = (email, password) =>
		setPersistence(auth, browserLocalPersistence).then(() =>
			createUserWithEmailAndPassword(auth, email, password)
		);

	const loginUser = (email, password) =>
		setPersistence(auth, browserLocalPersistence).then(() =>
			signInWithEmailAndPassword(auth, email, password)
		);

	// Aca deberiamos agregar:
	// - Estados compartidos
	// - Funciones para agregar datos
	// - etc...
	// Lo que queramos compartir en toda la aplicacion
	// debemos pasarlo al objeto value que enviamos en el provider
	return (
		<PotatoContext.Provider
			value={{
				globalData,
				setGlobalData,
				setUser,
				registerUser,
				loginUser,
				nextStep,
				prevStep,
				cancel,
				percentage,
			}}>
			{children}
		</PotatoContext.Provider>
	);
};

export { PotatoContext, PotatoProvider };
