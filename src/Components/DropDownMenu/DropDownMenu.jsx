import React  from 'react';
import {useState} from 'react';
import './DropDownMenu.css';
import { Link } from 'react-router-dom';

function DropDownMenu (){
    const [menu, setShowMenu] = useState(false);

    function showMenu (){
        setShowMenu( prevMenu => !prevMenu)
    }
    return(
       <nav className="nav">
            <input id="btnmenu" className="toggle" type="checkbox" onChange={showMenu}/>
                <span></span>
                <span></span>
                <span className='last-span'></span>
            {menu &&
                <ul className="menu">
                    <li><Link className="nav-item"  to="/about-us">About</Link></li>
                    <li><Link className="nav-item"  to="/faqs">FAQs</Link></li>
                    <li><Link className="nav-item"  to="/contact">Contact</Link></li>
                    <li className="separated--li"><a className="nav-item separated" href="/">Sign Out</a></li>
                </ul>
            }
        </nav>
    )
}
export default DropDownMenu;