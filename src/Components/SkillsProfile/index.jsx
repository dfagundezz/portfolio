import React from 'react';
import './SkillsProfile.css';

function SkillsProfile({ title, data }) {
	// percentage="25" skillName="Origami"
	//   const values = `calc(440 - (440 * ${percentage}) / 100)`;
	return (
		<div className="SectionProfileWrapper2">
			<div className="header">{title}</div>
			<div className="SkillsProfileWrapper">
				{data.map((value, key) => (
					<div key={value.id} className="box">
						<div className="percent">
							<svg>
								<circle cx="70" cy="70" r="70"></circle>
								<circle
									style={{
										strokeDashoffset: 440 - (440 * value.percentage) / 100,
									}}
									cx="70"
									cy="70"
									r="70"></circle>
							</svg>
							<div className="num">
								<h2>
									{value.percentage}
									<span>%</span>
								</h2>
							</div>
						</div>
						<h2 className="text">{value.name}</h2>
					</div>
				))}
			</div>
		</div>
	);
}

export { SkillsProfile };
