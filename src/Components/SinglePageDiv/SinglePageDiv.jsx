import React, { useState } from 'react';
import "./SinglePageDiv.css";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faYoutube,faFacebook,faTwitter,faInstagram} from "@fortawesome/free-brands-svg-icons";
import {Input} from '../Input/index';
import {ButtonSolid} from '../ButtonSolid/index';

function SinglePageDiv(props){
    const [form, setForm] = useState(false);

    function submitForm (e){
        e.preventDefault();
        setForm( prevForm => !prevForm)
    }
    return (
        <div className='single-page-container'>
            <div className='single-page-description'>
                <h1 className='single-page-title'>{props.title}</h1>
                <p className='single-page-text'>{props.text}</p>
            </div>
            {props.social &&
                <div className='contact-social-container'>
                    <div className='contact-form-social'>
                        <h3 className='title-social'>Send us a message:</h3>
                        <Input
                            input={{
                                placeholder: 'Full name',
                            }}
                            label=""
                            icon=""
                            phrase=""
                        ></Input>
                        <Input
                            input={{
                                placeholder: 'Message',
                            }}
                            label=""
                            icon=""
                            phrase=""
                        ></Input>
                        <ButtonSolid
                            text={!form ? "Submit" : "We'll keep in touch"}
                            type="submit"
                            callback={submitForm}>
                        </ButtonSolid>
                    </div>
                    <div className='social-media'>
                        <h3 className='title-social'>Or follow Us:</h3>
                        <ul className='contact-social'>
                            <li>
                                <a href="https://www.youtube.com/" target="_blank" rel="noopener noreferrer" className="youtube social">
                                    <FontAwesomeIcon icon={faYoutube} size="2x" />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer" className="facebook social">
                                    <FontAwesomeIcon icon={faFacebook} size="2x" />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.twitter.com/" target="_blank" rel="noopener noreferrer" className="twitter social">
                                    <FontAwesomeIcon icon={faTwitter} size="2x" />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/" target="_blank" rel="noopener noreferrer" className="instagram social">
                                    <FontAwesomeIcon icon={faInstagram} size="2x" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            }
            <button className='ButtonOutlineSingle'><Link to={props.link}>{props.btnName}</Link></button>
        </div>
    )
}
export default SinglePageDiv;