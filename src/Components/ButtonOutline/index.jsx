import React from 'react';
import './ButtonOutline.css';

function ButtonOutline({ text, icon, type, disabled, callback }) {
  return (
    <button
      className="ButtonOutline"
      onClick={callback}
      disabled={disabled}
      type={type ?? 'button'}
    >
      {icon && <i className={'fa ' + (icon ? icon : '')}></i>}
      <span>{text}</span>
    </button>
  );
}

export { ButtonOutline };
