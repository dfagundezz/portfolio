import React, { useState } from 'react';
import './FileInput.css';

const FileInput = ({ input, label, icon }) => {
	const [files, setFiles] = useState(null);
	const handleChange = (evt) => {
		evt.preventDefault();
		console.log(evt);
		console.log(evt.target.files);
		setFiles(evt.target.files.item(0));
	};

	return (
		<div className="input">
			{label && <label htmlFor={input.id}>{label}</label>}
			<div className="controls">
				{icon && (
					<label className="icon" htmlFor={input.id}>
						<i className={'fa ' + icon} aria-hidden="true"></i>
					</label>
				)}
				<strong className="placeholder">
					{files !== null ? files.name : input.placeholder}
				</strong>
				<label className="file-button" htmlFor={input.id}>
					Select file
				</label>
			</div>
			<input
				className="control"
				type="file"
				{...input}
				hidden={true}
				onChange={handleChange}
			/>
		</div>
	);
};

export default FileInput;
