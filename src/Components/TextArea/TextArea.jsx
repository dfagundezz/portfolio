import React from 'react';
import './TextArea.css';

const TextArea = ({ input, label, icon }) => {
	return (
		<div className="TextArea">
			{label && <label htmlFor={input.id}>{label}</label>}
			<div className="controls">
				{icon && (
					<label className="icon" htmlFor={input.id}>
						<i className={'fa ' + icon} aria-hidden="true"></i>
					</label>
				)}

				<textarea
					className="control"
					{...input}
					style={{
						resize:
							input.resize !== undefined ? input.resize : 'none',
					}}></textarea>
			</div>
		</div>
	);
};

export default TextArea;
