import React from 'react';
import "./Modal.css";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {Modal, Button} from 'react-bootstrap';

function ModalDisplay(props){
    return (
        <Modal {...props} size="lg" centered>
        <Modal.Header className="modal-header">
            <Modal.Title id="contained-modal-title-vcenter">{props.data.title}</Modal.Title>
            <Button className="justify-content-end btn-modal" onClick={props.onHide}><FontAwesomeIcon icon={faTimes} /></Button>
        </Modal.Header>
        <Modal.Body>
            <p className="modal-p">{props.data.text}</p>
        </Modal.Body>
    </Modal>
    )
}
export default ModalDisplay;