import React, { useState } from 'react';
import './Checkbox.css';

export const Checkbox = ({ icon, input, label, onChange = void 0 }) => {
	const [checked, setChecked] = useState(false);

	const handleCheck = (e) => {
		setChecked(!checked);
		onChange(e);
	};

	return (
		<label className="Checkbox" htmlFor={input.id}>
			<div
				className={'tick ' + (checked ? 'checked' : '')}
				role="checkbox"
				aria-checked={checked}>
				<i className="fa fa-check"></i>
			</div>
			{icon && <i className={'fa ' + icon}></i>}
			{label && <span className="label">{label}</span>}
			<input
				aria-hidden="true"
				hidden
				id={input.id}
				name={input.name}
				onChange={handleCheck}
				type="checkbox"
				value={checked}
			/>
		</label>
	);
};
