import  React from 'react';
import './contact.css';
import SinglePageDiv from '../../Components/SinglePageDiv/SinglePageDiv';

function Contact (){
    return(
        <>
            <SinglePageDiv
                title= "CONTACT US"
                text= "Contact us by social media or send us a message"
                link="/"
                btnName= "HOME"
                social= {true}
            />
        </>
    );
}
export default Contact;