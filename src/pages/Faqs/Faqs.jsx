import  React from 'react';
import './faqs.css';
import SinglePageDiv from '../../Components/SinglePageDiv/SinglePageDiv';

function Faqs (){
    return(
        <>
            <SinglePageDiv
                title= "FAQs"
                text= "Any question? You can check our frequently questions or just contact us. We are here to help!"
                link="/"
                btnName= "HOME"
            />
        </>
    );
}
export default Faqs;