import  React from 'react';
import SinglePageDiv from '../../Components/SinglePageDiv/SinglePageDiv';

function AboutUs (){
    return(
        <>
            <SinglePageDiv
                title= "ABOUT US"
                text= "We are Potato. Helping you create your portfolio for your next challenges"
                link="/"
                btnName= "HOME"
            />
        </>
    );
}
export default AboutUs;