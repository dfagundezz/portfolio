const data = {
	user: {
		id: '450489',
		fullName: 'Foo Bar',
		email: 'foo.bar@hex.com.ar',
		password: '1234',
		currentJob: 'Barista',
		about: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque aperiam inventore enim asperiores perspiciatis ipsam sapiente eos iusto, deleniti error impedit deserunt debitis nobis dicta, eveniet, soluta sequi culpa delectus!',
	},
	portfolio: {
		education: [
			{
				id: 1,
				place: 'Codo a Codo', //institution
				reference: 'React - Go', // title
				description: 'What can I say? It is free',
				image: 'https://i.pinimg.com/originals/7d/ab/99/7dab9996f15513443ab7ec4d1c04cab9.jpg',
				startDate: '20/03/2022',
				endDate: '30/06/2022 ',
				// certification: 'https://validcertificate.com',
				tags: ['#Typescript ', '#Percy ', '#Github '],
			},
		],
		experience: [
			{
				id: 1,
				place: 'XYZ Inc.',
				reference: 'Brainfuck Developer',
				description:
					'Wrote some scripts to automate the production of sliced bread.',
				image: 'https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png',
				startDate: '01/01/2001',
				endDate: '24/12/2015',
				tags: ['programming', 'Brainfuck'],
			},
			{
				id: 2,
				place: 'Alphabet Co.', // companyName
				reference: 'COBOL Dev Lead', // position
				description: "I don't know how I ended up here.",
				image: 'https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png',
				startDate: '05/10/2016',
				endDate: 'Present',
				tags: ['leadership', 'programming', 'cobol'],
			},
		],
		projects: [
			{
				id: 1,
				place: 'Personal',
				reference: 'The Fox Community', // projectName
				description:
					'A little website where you can pet foxes for free',
				image: 'googsssle.com/', // website
				startDate: '01/01/2001',
				endDate: '24/12/2015',
				tags: ['community', 'programming', 'foxes'],
			},
		],
		skills: [
			{
				id: 1,
				skillName: 'Origami',
				percentage: 15,
				type: 'Arts & Crafts',
			},
			{
				id: 2,
				skillName: 'Art',
				percentage: 80,
				type: 'Arts & Crafts',
			},
			{
				id: 3,
				skillName: 'Yoga',
				percentage: 65,
				type: 'Arts & Crafts',
			},
			{
				id: 4,
				skillName: 'Yoga',
				percentage: 65,
				type: 'Arts & Crafts',
			},
			{
				id: 5,
				skillName: 'Yoga',
				percentage: 65,
				type: 'Arts & Crafts',
			},
			{
				id: 6,
				skillName: 'Yoga',
				percentage: 65,
				type: 'Arts & Crafts',
			},
			{
				id: 7,
				skillName: 'Yoga',
				percentage: 65,
				type: 'Arts & Crafts',
			},
		],
	},
	error: false,
	loading: false,
	step: 1,
};
