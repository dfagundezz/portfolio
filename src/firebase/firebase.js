import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAk9r5Ru161IQGw0YGbTZ9mWHgHw0oWj68",
    authDomain: "potato-c8721.firebaseapp.com",
    projectId: "potato-c8721",
    storageBucket: "potato-c8721.appspot.com",
    messagingSenderId: "84016251608",
    appId: "1:84016251608:web:76dec70403169eaadb4dad"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);

export { auth };
